## Test Reign - Full Stack Javascript

### to run in Docker

1. You should rename the `.env.example` file to `.env` and set the environment variables for the **backend**, the default setting works fine in docker.
2. Only if you change the listen port of the **backend** should you also change it in **test-frontend/src/environments/environments.prod**.
3. Open a terminal, position yourself in the folder of this project and run `docker-compose up --build` ... and wait.
4. When the excution has completed you can navigate to [http://localhost/](http://localhost/)

For more details see README file in **test-backend/** and **test-frontend/**