import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

    transform(date: Date): unknown {

        let dateTransformed;


        if (typeof date !== 'undefined') {

            if (this.isLessThan24Hours(date)) {
                dateTransformed = new Date(date).toLocaleTimeString('en-US', {
                    hour: '2-digit',
                    minute: '2-digit'
                });
            } else if (this.isLessThan48Hours(date)) {
                dateTransformed = 'Yesterday';
            } else {
                dateTransformed = new Date(date).toLocaleDateString('en-US', {
                    month: 'short',
                    day: 'numeric'
                });
            }



        }


        return dateTransformed ? dateTransformed : date;
    }

    private isLessThan24Hours(date: Date) {

        const hours = this.getHoursTranscurred(date);

        if (hours < 24) {
            return true;
        } else {
            return false;
        }

    }

    private isLessThan48Hours(date: Date) {

        const hours = this.getHoursTranscurred(date);

        if (hours < 48) {
            return true;
        } else {
            return false;
        }

    }

    private getHoursTranscurred(date: Date) {

        const nowTimestamp = Date.now();
        const dateTimestamp = new Date(date).getTime();

        const dateDiff = nowTimestamp - dateTimestamp;

        const hours = dateDiff / (1000 * 3600);

        return hours;

    }

}
