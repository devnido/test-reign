import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ArticlesService } from '../../services/articles.service';
import { Subscription } from 'rxjs';
import { Article } from '../../models/article.model';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit, OnDestroy {

    dataSource: any;
    displayedColumns: any;
    subscriptions: Subscription[] = [];
    loading = false;
    loadingItem = '';

    constructor(private articlesService: ArticlesService) { }

    ngOnInit(): void {
        this.loadArticles();
    }
    ngOnDestroy(): void {
        this.subscriptions.map(s => s.unsubscribe());
    }

    loadArticles() {

        this.loading = true;
        const subscription = this.articlesService.getAll()
            .subscribe((resp: Article[]) => {
                console.log(resp);
                this.dataSource = resp;

                this.displayedColumns = ['titleAndAuthor', 'date', 'action'];
                this.loading = false;
            }, error => {
                this.loading = false;
                Swal.fire('An error has occurred', 'Try it again later', 'error');
                console.log(error);
            });

        this.subscriptions.push(subscription);
    }

    openUrl(article: Article) {

        const url = article.story_url ? article.story_url : article.url;

        window.open(url, '_blank');
    }

    deleteArticle(article: Article) {

        this.loadingItem = article._id;
        const subscription = this.articlesService.delete(article._id)
            .subscribe((resp: boolean) => {

                if (resp) {
                    this.loadArticles();
                }
                this.loadingItem = '';

            }, error => {
                this.loadingItem = '';
                Swal.fire('An error has occurred', 'Try it again later', 'error');
                console.log(error);
            });

        this.subscriptions.push(subscription);
    }

}
