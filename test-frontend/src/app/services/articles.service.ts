import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ArticlesService {
    baseUrl = environment.baseUrlApi;

    constructor(private http: HttpClient) { }


    getAll() {


        const url = this.baseUrl + '/articles';

        return this.http.get(url)
            .pipe(
                map((resp: any) => {

                    return resp.content.articles;
                })
            );
    }

    delete(id: string) {

        const url = this.baseUrl + '/articles/' + id;

        return this.http.delete(url)
            .pipe(
                map((resp: any) => {

                    return resp.content;
                })
            );
    }


}
