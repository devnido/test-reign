export class Article {
    _id?: string;
    story_title?: string;
    title?: string;
    story_url?: string;
    url?: string;
    author?: string;
    created_at?: Date;

    constructor() {
    }
}
