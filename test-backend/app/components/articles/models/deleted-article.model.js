const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const deletedArticle = new Schema({
    objectID: String
}, {
    timestamps: true
})

const DeletedArticle = mongoose.model('deletedArticle', deletedArticle);

module.exports = DeletedArticle;