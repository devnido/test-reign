const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const article = new Schema({
    story_title: String,
    title: String,
    story_url: String,
    url: String,
    author: String,
    created_at: Date,
    objectID: String
}, {
    timestamps: true
})

const Article = mongoose.model('article', article);

module.exports = Article;