const mapper = {

    discardWithoutTitle: (hits) => {
        return hits.filter(hit => hit.story_title || hit.title)
    },

    discardWithoutUrl: (hits) => {
        return hits.filter(hit => hit.story_url || hit.url)
    },

    articlesResponseToArticles: (hits) => {

        const articles = hits.reduce((arr, hit) => arr.concat({
            story_title: hit.story_title,
            title: hit.title,
            story_url: hit.story_url,
            url: hit.url,
            author: hit.author,
            created_at: hit.created_at,
            objectID: hit.objectID
        }), [])

        return articles
    },

    excludeDeletedArticles: (articles, deletedArticles) => {

        const indexedDeletedArticles = deletedArticles.reduce((arr, article) => ({...arr, [article.objectID]: article }), {})

        const articlesAllowed = articles.filter(article => typeof indexedDeletedArticles[article.objectID] === 'undefined')

        return articlesAllowed;

    },

    sortArticlesByDateAsc: (articles) => {

        sortedArticles = articles.sort((a, b) => new Date(a.created_at) - new Date(b.created_at))

        return sortedArticles
    },

    excludeExistingArticles: (articles, existingArticles) => {

        const indexedExistingArticles = existingArticles.reduce((arr, article) => ({...arr, [article.objectID]: article }), {})

        const articlesToSave = articles.filter(article => typeof indexedExistingArticles[article.objectID] === 'undefined')

        return articlesToSave;
    }

}

module.exports = mapper