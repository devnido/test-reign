const axios = require('axios')


const service = ({ config }) => ({

    getArticlesFromHackerNews: async() => {

        const resourceUrl = `${config.externalApi.hackerNewsUrl }${config.externalApi.hackerNewsResource}${config.externalApi.hackerNewsParams}`

        const response = await axios.get(resourceUrl)

        if (response && response.data && response.data.hits) {
            return response.data.hits
        }

        throw new Error(`can't connect to ${config.externalApi.hackerNewsUrl }`)
        return;
    }

})

module.exports = service;