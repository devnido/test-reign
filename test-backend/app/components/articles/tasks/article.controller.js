const controller = ({ articleTaskMapper, articleTaskService, articleTaskRepository }) => ({


    getDataFromApiAndInsertInDb: async() => {
        const [
            hits,
            deletedArticles
        ] = await Promise.all([
            articleTaskService.getArticlesFromHackerNews(),
            articleTaskRepository.getAllDeleted()
        ]);

        // The logic programmed below aims to avoid making queries to the database within loops and nested loops

        const hitsWithTitle = articleTaskMapper.discardWithoutTitle(hits)

        const hitsWithUrl = articleTaskMapper.discardWithoutUrl(hitsWithTitle)

        const articles = articleTaskMapper.articlesResponseToArticles(hitsWithUrl)

        const articlesAllowed = articleTaskMapper.excludeDeletedArticles(articles, deletedArticles)

        // I will only compare articles with a date equal to or greater than the date of the item with a lower date in the list returned by hn api

        const articlesSorted = articleTaskMapper.sortArticlesByDateAsc(articles)

        const firstArticle = articlesSorted[0]

        const articlesInDB = await articleTaskRepository.getAllFromDate(firstArticle.created_at)

        const articlesToSave = articleTaskMapper.excludeExistingArticles(articlesAllowed, articlesInDB)

        const result = await articleTaskRepository.insertMany(articlesToSave)

        return result
    }

})

module.exports = controller