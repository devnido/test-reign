const cron = require('node-cron')


const cronTask = ({ articleTaskController }) => ({

    startAndRepeatEveryHour: () => {

        // execute at Start
        articleTaskController.getDataFromApiAndInsertInDb()
            .then(result => {})
            .catch(e => console.log(e))

        cron.schedule('0 0 * * * *', () => {

            // execute every hour 
            articleTaskController.getDataFromApiAndInsertInDb()
                .then(result => {})
                .catch(e => console.log(e))
        })
    }
})

module.exports = cronTask