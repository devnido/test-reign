const repository = ({ Article, DeletedArticle }) => ({


    getAllFromDate: date => Article.find({ created_at: { $gte: date } }),

    insertMany: articles => Article.insertMany(articles),

    getAllDeleted: () => DeletedArticle.find({})

})

module.exports = repository;