const repository = ({ Article, DeletedArticle }) => ({

    existsById: id => Article.exists({ _id: id }),

    getAll: () => Article.find({}).sort({ created_at: 'desc' }),

    getById: _id => Article.findOne({ _id }),

    delete: id => Article.deleteOne({ _id: id }),

    insertToDeletedList: objectID => DeletedArticle.create({ objectID })

})

module.exports = repository;