const { check, param, validationResult } = require('express-validator');

const validator = ({ articleApiRepository, errorHandler }) => ({
    delete: [
        check(['id']).trim().escape(),

        param('id')
        .exists({
            checkFalsy: true
        }).withMessage('Id is required')
        .isMongoId().withMessage('Id has incorrect format')
        .custom((id) => {

            return articleApiRepository.existsById(id)
                .then(result => {
                    if (!result) {
                        return Promise.reject('Article doesnt exist')
                    }
                })
                .catch(err => {
                    throw new Error('Article doesnt exist')
                })
        }).withMessage('Article doesnt exist'),

        errorHandler.validation(validationResult)
    ]
})

module.exports = validator