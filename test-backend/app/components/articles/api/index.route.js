const routes = ({ articleApiValidator, articleApiRoutes }) => ({

    init: (router) => {

        router.route('/articles').get(articleApiRoutes.get)

        router.route('/articles/:id').delete(articleApiValidator.delete, articleApiRoutes.delete)
    }

})


module.exports = routes;