const controller = ({ articleApiRepository }) => ({

    getAllFromDb: async() => {

        const articles = await articleApiRepository.getAll();

        const total = articles.length;

        return { articles, total }

    },

    deleteFromDb: async(id) => {

        const articleToBeRemoved = await articleApiRepository.getById(id)

        const deleted = await articleApiRepository.delete(id)

        if (deleted && deleted.deletedCount) {

            const added = await articleApiRepository.insertToDeletedList(articleToBeRemoved.objectID)

            return !!added;
        }

        return false;

    }
})

module.exports = controller