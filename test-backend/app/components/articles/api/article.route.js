const routes = ({ articleApiController }) => ({

    get: async(req, res, next) => {
        try {

            const { articles, total } = await articleApiController.getAllFromDb();


            const response = {
                ok: true,
                content: {
                    message: 'Articles List',
                    articles,
                    total
                }
            }


            res.status(200).json(response);

        } catch (error) {

            next({
                error: error,
                status: 500
            });
        }
    },
    delete: async(req, res, next) => {

        const { id } = req.params;

        try {

            const deleted = await articleApiController.deleteFromDb(id);

            const response = {
                ok: true,
                content: {
                    message: 'Article deleted',
                    result: deleted
                }
            }

            res.status(200).json(response);

        } catch (error) {
            next({
                error: error,
                status: 500
            })
        }
    }
})


module.exports = routes;