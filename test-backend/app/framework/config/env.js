const config = {
    app: {
        name: process.env.APP_NAME,
        env: process.env.NODE_ENV,
        ip: process.env.APP_LOCAL_IP,
        port: process.env.APP_LOCAL_PORT,
        urlApiPrefix: process.env.APP_PREFIX_API_URL,
        urlVersionPrefix: process.env.APP_VERSION_API
    },
    db: {
        api: {
            driver: process.env.DB_API_DRIVER,
            host: process.env.DB_API_HOST,
            port: process.env.DB_API_PORT,
            user: process.env.DB_API_USERNAME,
            pass: process.env.DB_API_PASSWORD,
            database: process.env.DB_API_DATABASE
        }
    },
    externalApi: {
        hackerNewsUrl: process.env.API_URL_HN,
        hackerNewsResource: process.env.API_URL_HN_RESOURCE,
        hackerNewsParams: process.env.API_URL_HN_PARAMS
    }
};

module.exports = config;