const routes = ({ config, indexArticleApiRoute }) => ({
    init: (express, app) => {
        var router = express.Router()

        // routes list 
        indexArticleApiRoute.init(router)

        // url prefix
        const prefixApiUrl = config.app.urlApiPrefix // api/
        const prefixVersionApi = config.app.urlVersionPrefix // v1/

        const prefix = `${prefixApiUrl}${prefixVersionApi}` // api/v1

        // http://localhost:3000/api/v1/ 
        app.use(prefix, router)
    }
})

module.exports = routes