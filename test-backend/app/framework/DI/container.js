const { createContainer, asFunction, asValue, asClass, InjectionMode } = require('awilix')

// articles - models
const Article = require("../../components/articles/models/article.model");
const DeletedArticle = require("../../components/articles/models/deleted-article.model");

// artciles - api
const articleApiRepository = require('../../components/articles/api/article.repository')
const articleApiController = require('../../components/articles/api/article.controller')
const articleApiRoutes = require('../../components/articles/api/article.route')
const articleApiValidator = require('../../components/articles/api/article.validation')
const indexArticleApiRoute = require('../../components/articles/api/index.route')

// artciles - tasks
const articleTaskRepository = require('../../components/articles/tasks/article.repository')
const articleTaskController = require('../../components/articles/tasks/article.controller')
const articleTaskService = require('../../components/articles/tasks/article.service')
const articleTaskMapper = require('../../components/articles/tasks/article.mapper')
const articleTaskCron = require('../../components/articles/tasks/article.cron')

const config = require('../../framework/config/env');
const errorHandler = require('../middlewares/error-handler.middleware');

const routes = require('../routes/main.route')


const container = createContainer();

container
    .register({
        config: asValue(config),
        errorHandler: asValue(errorHandler),
        routes: asFunction(routes).singleton()
    })
    .register({
        articleApiRepository: asFunction(articleApiRepository).singleton(),
        articleApiController: asFunction(articleApiController).singleton(),
        articleApiRoutes: asFunction(articleApiRoutes).singleton(),
        articleApiValidator: asFunction(articleApiValidator).singleton(),
        indexArticleApiRoute: asFunction(indexArticleApiRoute).singleton()
    })
    .register({
        articleTaskRepository: asFunction(articleTaskRepository).singleton(),
        articleTaskController: asFunction(articleTaskController).singleton(),
        articleTaskService: asFunction(articleTaskService).singleton(),
        articleTaskCron: asFunction(articleTaskCron).singleton(),
        articleTaskMapper: asValue(articleTaskMapper)
    })
    .register({
        Article: asValue(Article),
        DeletedArticle: asValue(DeletedArticle)
    })

module.exports = container;