var mongoose = require('mongoose');
var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

const chai = require('chai');
const expect = require('chai').expect;
const assert = require('chai').assert;

const { FromDb } = require('./mocks/articles.mock').init()
const Article = require('../app/components/articles/models/article.model')
const DeletedArticle = require('../app/components/articles/models/deleted-article.model')
const articleRepository = require('../app/components/articles/api/article.repository')
const repository = articleRepository({ Article, DeletedArticle })

describe('Testing articles api repository', function() {


    it("Should return a true if article exists", function(done) {

        const articleMock = FromDb.article

        repository.existsById(articleMock._id)
            .then(result => {

                expect(result).equals(true)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should return a false if article not exists", function(done) {

        const articleMock = FromDb.article

        repository.existsById("4baa56f1230048567300485d")
            .then(result => {

                expect(result).equals(false)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should return an article searched by id", function(done) {

        const articleMock = FromDb.article

        repository.getById(articleMock._id)
            .then(article => {

                // expect(article).to.have.property('objectID').that.equal(articleMock);
                expect(JSON.parse(JSON.stringify(article)))
                    .have.property('_id')
                    .to.be.equal(articleMock._id, 'Articles are equals')
                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should return a list of articles", function(done) {

        const articlesMock = FromDb.articles

        repository.getAll()
            .then(articles => {

                expect(articles)
                    .to.be.an('array')
                    .that.lengthOf(articlesMock.length)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should remove an item from the list", function(done) {

        const articleMock = FromDb.article

        repository.delete(articleMock._id)
            .then(result => {

                expect(result).have.property('deletedCount').to.be.equal(1)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should insert in deleted articles collection", function(done) {

        const articleMock = FromDb.article

        repository.insertToDeletedList(articleMock.objectID)
            .then(result => {

                expect(result).have.property('objectID').to.be.equal(articleMock.objectID)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });


    before('database connect', function(done) {
        mockgoose.prepareStorage().then(function() {
            mongoose.connect('mongodb://example.com/TestingDB', {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                useUnifiedTopology: true
            }, function(err) {
                done(err)
            });
        });
    });

    after('database disconnect', function(done) {

        mongoose.disconnect()
            .then(() => {

                return mockgoose.shutdown()

            })
            .then(() => {
                done()
            })
            .catch(err => {
                done(err)
            })


    });

    beforeEach('populate articles collection', function(done) {

        function populateArticles(articles) {

            Article.insertMany(articles)
                .then(result => {
                    done()
                })
                .catch(e => {
                    console.log(e);
                    done(e)
                })
        }

        const { articles } = FromDb

        populateArticles(articles)

    });

    afterEach('clean articles collection', function(done) {

        Article.deleteMany({})
            .then(result => {

                return DeletedArticle.deleteMany({})

            })
            .then(result => {
                done()
            })
            .catch(err => {
                done(err)
            });
    });





});