const chai = require('chai')
const expect = require('chai').expect
const sinon = require('sinon')


const articlesMapper = require('../app/components/articles/tasks/article.mapper');

const { FromDb, FromApi } = require('./mocks/articles.mock').init()

describe('Testing articles tasks mapper', function() {

    it('Should return only articles with title or story_title', function() {

        const { hits, hitsWOTitle } = FromApi

        const result = articlesMapper.discardWithoutTitle(hits)


        const diff = hits.length - result.length

        expect(diff).to.be.equal(hitsWOTitle.length)
        expect(result).to.be.an('array').that.not.contains(hitsWOTitle[0])
        expect(result).to.be.an('array').that.not.contains(hitsWOTitle[1])
        expect(result).to.be.an('array').that.not.contains(hitsWOTitle[2])

    })

    it('Should return only articles with title or story_title', function() {

        const { hits, hitsWOUrl } = FromApi

        const result = articlesMapper.discardWithoutUrl(hits)

        const diff = hits.length - result.length


        expect(diff).to.be.equal(hitsWOUrl.length)
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[0])
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[1])
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[2])

    })

    it('Should return only articles with url or story_url', function() {

        const { hits, hitsWOUrl } = FromApi

        const result = articlesMapper.discardWithoutUrl(hits)

        const diff = hits.length - result.length

        expect(diff).to.be.equal(hitsWOUrl.length)
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[0])
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[1])
        expect(result).to.be.an('array').that.not.contains(hitsWOUrl[2])

    })

    it('Should transform a list of articles', function() {

        const { hits } = FromApi

        const result = articlesMapper.articlesResponseToArticles(hits)

        expect(result).to.be.an('array').lengthOf(hits.length)

        expect(result[0]).that.not.have
            .a.property('story_id')
        expect(result[0]).that.not.have
            .a.property('comment_text')
        expect(result[0]).that.not.have
            .a.property('num_comments')

        expect(result[0]).that.have.a.property('objectID')

    })

    it('Should exclude items from a list', function() {

        let { articles, deletedArticles } = FromDb

        let articlesNotDeleted = []

        articlesNotDeleted.push(deletedArticles.shift())
        articlesNotDeleted.push(deletedArticles.shift())
        articlesNotDeleted.push(deletedArticles.shift())
        articlesNotDeleted.push(deletedArticles.shift())
        articlesNotDeleted.push(deletedArticles.shift())


        const result = articlesMapper.excludeDeletedArticles(articles, deletedArticles)

        expect(articlesNotDeleted.length).to.be.equals(result.length)


    })

    it('Should sort a list of articles by date asc', function() {

        const { articles, sortedArticles } = FromDb

        const sortedArticlesAsc = articlesMapper.sortArticlesByDateAsc(articles)

        expect(sortedArticlesAsc).equals(sortedArticles.reverse())

    })

    it('Should delete the articles existing in another list of articles', function() {

        const { articles } = FromDb

        const existingArticles = articles.slice(0, 4)

        const result = articlesMapper.excludeExistingArticles(articles, existingArticles)

        expect(result).not.contains(existingArticles[0])

    })




})