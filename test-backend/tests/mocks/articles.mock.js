const fs = require('fs');
const path = require('path');

const mock = {
    init: () => {

        const hitsFromApiString = fs.readFileSync(path.join(__dirname, 'articles/articles-api.json'));
        const hitsFromApi = JSON.parse(hitsFromApiString)

        const hitsWithoutTitleAndStoryTitle = hitsFromApi.hits.filter(h => !h.title && !h.story_title)
        const hitsWithoutUrlAndStoryUrl = hitsFromApi.hits.filter(h => !h.url && !h.story_url)

        const articlesFromDbString = fs.readFileSync(path.join(__dirname, 'articles/articles-db.json'));
        const articlesFromDb = JSON.parse(articlesFromDbString)

        const sortedArticlesDesc = articlesFromDb.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))

        deletedArticles = articlesFromDb.reduce((arr, value) => arr.concat({ objectID: value.objectID }), [])

        return {
            FromApi: {
                hits: hitsFromApi.hits,
                hitsWOTitle: hitsWithoutTitleAndStoryTitle,
                hitsWOUrl: hitsWithoutUrlAndStoryUrl,
                hit: hitsFromApi.hits[0]
            },
            FromDb: {
                articles: articlesFromDb,
                article: articlesFromDb[0],
                sortedArticles: sortedArticlesDesc,
                deletedArticles
            }
        }
    }
}

module.exports = mock