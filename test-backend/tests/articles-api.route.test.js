const chai = require('chai')
const expect = require('chai').expect
const sinon = require('sinon')


let articleApiRoute = require('../app/components/articles/api/article.route')
const { FromDb } = require('./mocks/articles.mock').init()



describe('Testing articles api routes', () => {

    let controllerMock
    let routes
    let articlesMock
    let articleMock
    let reqMock, resMock
    let responseMock


    beforeEach('Create mocks', function() {

        articlesMock = FromDb.articles
        articleMock = FromDb.article


        controllerMock = {
            getAllFromDb: sinon.stub().resolves({ articles: articlesMock, total: articlesMock.length }),
            deleteFromDb: sinon.stub().withArgs(1).resolves(true)
        }

        routes = articleApiRoute({ articleApiController: controllerMock })

        reqMock = { params: { id: 1 } }

        resMock = () => {
            const res = {}
            res.status = sinon.stub().returns(res)
            res.json = sinon.stub().returns(res)
            return res
        };

    })

    afterEach('Restore sinon', function() {

        controllerMock = null
        controller = null
        articlesMock = null
        articleMock = null
        reqMock = null
        resMock = null
        responseMock = null

        sinon.restore()

    })

    describe('route - /get', function() {

        it('Should take articles from db', function(done) {

            const req = reqMock
            const res = resMock()
            const next = () => {}

            routes.get(req, res, next)
                .then(result => {

                    expect(controllerMock.getAllFromDb.called).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })


        })

        it('Should return response with status 200', function(done) {

            const req = reqMock
            const res = resMock()
            const next = () => {}



            routes.get(req, res, next)
                .then(result => {

                    expect(res.status.calledWith(200)).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })
        })

        it('Should return response with json data ', function(done) {

            const req = reqMock
            const res = resMock()
            const next = () => {}

            responseMock = {
                ok: true,
                content: {
                    message: 'Articles List',
                    articles: articlesMock,
                    total: articlesMock.length
                }
            }

            routes.get(req, res, next)
                .then(result => {

                    expect(res.json.calledWith(responseMock)).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })
        })

    });

    describe('route - /delete', function() {


        it('Should delete an article from db by id', function(done) {

            const req = reqMock
            const res = resMock()
            const next = () => {}

            routes.delete(req, res, next)
                .then(result => {

                    expect(controllerMock.deleteFromDb.calledWith(1)).equals(true)

                    done()

                })
                .catch(e => {
                    console.log(e)
                    done(e)

                })
        })



        it('Should return response with status 200', function(done) {


            const req = reqMock
            const res = resMock()
            const next = () => {}


            routes.delete(req, res, next)
                .then(result => {

                    expect(res.status.calledWith(200)).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })
        })

        it('Should return res with json data', function(done) {


            const req = reqMock
            const res = resMock()
            const next = () => {}


            responseMock = {
                ok: true,
                content: {
                    message: 'Article deleted',
                    result: true
                }
            }

            routes.delete(req, res, next)
                .then(result => {


                    expect(res.json.calledWith(responseMock)).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })
        })

        it('Should return res result false when id does not exists', function(done) {

            controllerMock.deleteFromDb = sinon.stub().withArgs(13).resolves(false)

            const req = { params: { id: 13 } }
            const res = resMock()
            const next = () => {}


            responseMock = {
                ok: true,
                content: {
                    message: 'Article deleted',
                    result: false
                }
            }

            routes.delete(req, res, next)
                .then(result => {

                    expect(res.json.calledWith(responseMock)).equals(true)

                    done()
                })
                .catch(e => {
                    console.log(e)
                    done(e)
                })
        })

    });

});