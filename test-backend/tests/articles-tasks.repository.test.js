var mongoose = require('mongoose');
var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

const chai = require('chai');
const expect = require('chai').expect;
const assert = require('chai').assert;

const { FromDb } = require('./mocks/articles.mock').init()
const Article = require('../app/components/articles/models/article.model')
const DeletedArticle = require('../app/components/articles/models/deleted-article.model')
const articleRepository = require('../app/components/articles/tasks/article.repository')
const repository = articleRepository({ Article, DeletedArticle })

describe('Testing articles tasks repository', function() {


    it("Should return a list of articles from date ", function(done) {

        const sortedMock = FromDb.sortedArticles

        const date = sortedMock[5].created_at //date of 6th element 

        repository.getAllFromDate(date)
            .then(result => {

                expect(result).to.be.an('array')
                    .that.lengthOf(6)

                done()
            })
            .catch(e => {
                console.log(e)
                done(e)
            })
    });

    it("Should add articles to db", function(done) {

        const articlesMock = FromDb.articles

        Article.deleteMany({})
            .then(s => {

                repository.insertMany(articlesMock)
                    .then(result => {

                        expect(result).to.be.an('array')
                            .that.lengthOf(articlesMock.length)

                        done()
                    })
                    .catch(e => {
                        console.log(e)
                        done(e)
                    })
            })


    });


    it('Should return a list of deleted articles', function(done) {


        const deletedArticlesMock = FromDb.deletedArticles

        repository.getAllDeleted()
            .then(articles => {

                expect(articles).to.be.an('array')
                    .that.lengthOf(deletedArticlesMock.length)

                done()

            }).catch(e => {
                console.log(e)
                done(e)
            })


    })

    before('database connect', function(done) {
        mockgoose.prepareStorage().then(function() {
            mongoose.connect('mongodb://example.com/TestingDB', {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                useUnifiedTopology: true
            }, function(err) {
                done(err)
            });
        });
    });

    after('database disconnect', function(done) {

        mongoose.disconnect()
            .then(() => {

                return mockgoose.shutdown()

            })
            .then(() => {
                done()
            })
            .catch(err => {
                done(err)
            })
    });

    beforeEach('populate articles collection', function(done) {

        function populateArticles(articles, deletedArticles) {

            Article.insertMany(articles)
                .then(result => {
                    return DeletedArticle.insertMany(deletedArticles)
                })
                .then(result => {
                    done()
                })
                .catch(e => {
                    console.log(e);
                    done(e)
                })
        }


        const { articles, deletedArticles } = FromDb

        populateArticles(articles, deletedArticles)

    });

    afterEach('clean articles collection', function(done) {

        Article.deleteMany({})
            .then(result => {

                return DeletedArticle.deleteMany({})

            })
            .then(result => {
                done()
            })
            .catch(err => {
                done(err)
            });
    });





});