const chai = require('chai')
const expect = require('chai').expect
const sinon = require('sinon')


let articleApiController = require('../app/components/articles/api/article.controller')
const { FromDb } = require('./mocks/articles.mock').init()



describe('Testing articles api controller', () => {

    let repositoryMock
    let controller
    let articlesMock
    let articleMock

    beforeEach('Create mocks', function() {

        articlesMock = FromDb.articles
        articleMock = FromDb.article



        repositoryMock = {
            getAll: sinon.stub().resolves(articlesMock),
            getById: sinon.stub().withArgs(2).resolves("123456"),
            delete: sinon.stub().withArgs(2).resolves({ n: 1, ok: 1, deletedCount: 1 }),
            insertToDeletedList: sinon.stub().withArgs("123456").resolves({ objectID: "123456" })
        }

    })

    afterEach('Restore mocks', function() {

        repositoryMock = null
        controller = null
        articlesMock = null
        articleMock = null

        sinon.restore();

    })


    it('Should return a list of articles with the total number of records ', function(done) {

        controller = articleApiController({ articleApiRepository: repositoryMock })

        controller.getAllFromDb()
            .then(result => {

                expect(repositoryMock.getAll.called);

                expect(result).to.have.a.property('total').that.equals(result.articles.length)

                done()

            })
            .catch(e => {
                console.log(e);
                done(e)

            })

    })

    it('should return true if the article has been removed', function(done) {

        controller = articleApiController({ articleApiRepository: repositoryMock })

        controller.deleteFromDb(2)
            .then(result => {

                expect(repositoryMock.insertToDeletedList.called).equals(true)

                expect(result).equals(true)

                done()

            })
            .catch(e => {
                console.log(e);
                done(e)

            })
    })

    it('should return false if the item has not been removed', function(done) {

        controller = articleApiController({ articleApiRepository: repositoryMock })

        repositoryMock.delete = sinon.stub().withArgs(3).resolves(false)

        controller.deleteFromDb(3)
            .then(result => {

                expect(repositoryMock.insertToDeletedList.called).equals(false)

                expect(result).equals(false)

                done()

            })
            .catch(e => {
                console.log(e);
                done(e)

            })
    })

});