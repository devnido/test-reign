## Test Backend Solved

### This project was created to solve a test sent by reign.

To run this project on your computer in developer mode you must follow the following steps and have installed **Node.js v12** and **MongoDB v4.2**:

1. Copy the source code to your computer
2. Install the dependencies with `npm install`
3. Set environment variables in `.env` file
4. Start the `mongod` service for the mongodb database
5. In the term, in the project folder run `npm install` and then `npm run dev`
6. (Optional) to run tests run `npm test`

### Comments

I use this file structure because it seems easy to maintain and scale for me.
In this project I used dependency injection to write the cleanest code possible and to facilitate the writing of tests as a support library I used [awilix](https://github.com/jeffijoe/awilix)

### File Structure

I will describe the file structure

<pre>
test-backend
├─ <b>.dockerignore</b> -> ignore files for docker
├─ <b>.env</b> -> Environment Variables
├─ <b>.env.example</b> ->  example environment variables
├─ <b>.gitignore</b> 
├─ <b>app</b> ->  contains all the source code of the application
│  ├─ <b>components</b> ->  here are added the components or "entities" in the app
│  │  └─ <b>articles</b> ->  entity
│  │     ├─ <b>api</b> ->  contains the source code to implement the api rest
│  │     │  ├─ <b>article.controller.js</b> -> contains the business logic for api rest
│  │     │  ├─ <b>article.repository.js</b> -> access to data source (mongodb)
│  │     │  ├─ <b>article.route.js</b> -> express route handler
│  │     │  ├─ <b>article.validation.js</b> -> data entry validation
│  │     │  └─ <b>index.route.js</b> -> indexes express routes
│  │     ├─ <b>models</b> ->  contains the models from the database
│  │     │  ├─ <b>article.model.js</b> 
│  │     │  └─ <b>deleted-article.model.js</b> 
│  │     └─ <b>tasks</b> -> contains the code responsible for updating every hour
│  │        ├─ <b>article.controller.js</b> -> contains the business logic of the task module
│  │        ├─ <b>article.cron.js</b> -> run the cron job on the server
│  │        ├─ <b>article.mapper.js</b> -> transform the data from the api to an array of objects that the app will use
│  │        ├─ <b>article.repository.js</b> -> access to the mongodb data source
│  │        └─ <b>article.service.js</b> -> communication with external services in this case hacker news
│  └─ <b>framework</b> -> contains the source code that allows the application to work
│     ├─ <b>config</b> 
│     │  └─<b> env.js</b> -> environment variables are stored here
│     ├─ <b>database</b> 
│     │  └─ <b>db.connect.js</b> -> connection to the database
│     ├─ <b>DI</b> 
│     │  └─ <b>container.js</b> -> dependency injection with awilix
│     ├─ <b>middlewares</b> 
│     │  └─ <b>error-handler.middleware.js</b> -> api error handler
│     └─ <b>routes</b> 
│        └─ <b>main.route.js</b> -> api routes
├─ <b>app.js</b> -> bootstrap file
├─ <b>docker-compose.yml</b> -> to run with docker
├─ <b>Dockerfile</b> -> configuration file for docker
├─ <b>package-lock.json</b> 
├─ <b>package.json</b> 
├─ <b>README.md</b> 
└─ <b>tests</b> -> folder containing all written tests
   ├─ <b>articles-api.controller.test.js</b> 
   ├─ <b>articles-api.repository.test.js</b> 
   ├─ <b>articles-api.route.test.js</b> 
   ├─ <b>articles-tasks.mapper.test.js</b> 
   ├─ <b>articles-tasks.repository.test.js</b> 
   └─ <b>mocks</b> 
      ├─ <b>articles</b> 
      │  ├─ <b>articles-api.json</b> 
      │  └─ <b>articles-db.json</b> 
      └─ <b>articles.mock.js</b> 
</pre>


